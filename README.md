# README #

This is the Godot project for _TCKondLife: Persons reloaded_, a VR experience to know the people working in The Cocktail.

An initial functional description of the project in spanish (sorry!!) is just below.

Please, feel free to use the contact info for any question or issue you have. Thanks!!

### TckondLife: Persons reloaded

* TCKondLife nos permitirá conocer el camino desde nuestro puesto de trabajo hasta cualquier compañera o compañero que queramos en... The Cocktail Global!!. (vaaaaaale, inicialmente sólo en The Cocktail Madrid, pero tiempo al tiempo!! ⏳)
* Dependiendo de la oficina existirán diferentes anfitrionas que nos servirán de guía durante nuestra visita virtual.
* Amebia (nuestra inteligencia líquida "personificada"), será la única anfitriona que estará disponible en cualquier oficina.
* Cuando nos pongamos las gafas la primera escena será la propia Sala de VR, desde allí la anfitriona que elijamos nos llevará hasta nuestro sitio en Tck (o hasta la compi o el compi que queramos!! :D).
* Como hito futuro podremos viajar en el tiempo para poder recuperar eternamente a Maggie en Madrid para realizar la visita (<3<3<3).

### Contact info ###

* Project members: https://bitbucket.org/lesvrengadores/profile/members
* Project contact: fernando.garcia at the-cocktail.com
